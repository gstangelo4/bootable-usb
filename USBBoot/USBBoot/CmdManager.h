// Greg St. Angelo IV
// 7.22.2017
#pragma once
#include <windows.h>
#include <stdio.h>
#include <vector>
#include <iostream>
#include <fstream>

namespace USBBoot
{
	class CmdManager
	{
	public:
		// Constructor
		CmdManager();
		~CmdManager();

		void AddCommand(char* aCommand);
		void LogAllCommands();
		void Exec();
		char* GetAllCommands();

		// vector of strings to be used for commands
		typedef std::vector<char*> Command;
	private:
		Command* m_Commands;
		std::ofstream m_Stream;
	};
}