#include "CmdManager.h"

USBBoot::CmdManager::CmdManager()
{
	m_Commands = new Command();
}

USBBoot::CmdManager::~CmdManager()
{
	delete	m_Commands;
}

void USBBoot::CmdManager::AddCommand(char* aCommand)
{
	m_Commands->push_back(aCommand);
}

void USBBoot::CmdManager::LogAllCommands()
{
	for (auto& itr : *(m_Commands))
	{
		std::cout << itr;
	}
}

void USBBoot::CmdManager::Exec()
{
	if (m_Commands != nullptr)
	{
		m_Stream.open("commands.txt");
		for (auto& itr : *(m_Commands))
		{
			m_Stream << itr << std::endl;
		}
		m_Stream.close();

		system("diskpart /s commands.txt"); // find this file based on project directory
	}
}

char * USBBoot::CmdManager::GetAllCommands()
{
	if (m_Commands != nullptr)
	{
		char* _out = "";
		for (auto& itr : *(m_Commands))
		{
			_out = (*(_out)+itr); // lol nice
		}
		return _out;
	}
	return nullptr;
}
