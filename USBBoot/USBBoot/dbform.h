#pragma once
#include "CmdManager.h"

namespace USBBoot
{
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for dbform
	/// </summary>
	public ref class dbform : public System::Windows::Forms::Form
	{
	public:
		dbform(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~dbform()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  m_Exit;
	private: System::Windows::Forms::Button^  m_Run;
	private: System::Windows::Forms::Label^  m_TestLabel;
	protected:

	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->m_Exit = (gcnew System::Windows::Forms::Button());
			this->m_Run = (gcnew System::Windows::Forms::Button());
			this->m_TestLabel = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// m_Exit
			// 
			this->m_Exit->Location = System::Drawing::Point(245, 294);
			this->m_Exit->Name = L"m_Exit";
			this->m_Exit->Size = System::Drawing::Size(114, 53);
			this->m_Exit->TabIndex = 0;
			this->m_Exit->Text = L"&Exit";
			this->m_Exit->UseVisualStyleBackColor = true;
			this->m_Exit->Click += gcnew System::EventHandler(this, &dbform::m_Exit_Click);
			// 
			// m_Run
			// 
			this->m_Run->Location = System::Drawing::Point(12, 294);
			this->m_Run->Name = L"m_Run";
			this->m_Run->Size = System::Drawing::Size(114, 53);
			this->m_Run->TabIndex = 1;
			this->m_Run->Text = L"&Run";
			this->m_Run->UseVisualStyleBackColor = true;
			this->m_Run->Click += gcnew System::EventHandler(this, &dbform::m_Run_Click);
			// 
			// m_TestLabel
			// 
			this->m_TestLabel->AutoSize = true;
			this->m_TestLabel->Location = System::Drawing::Point(12, 9);
			this->m_TestLabel->Name = L"m_TestLabel";
			this->m_TestLabel->Size = System::Drawing::Size(180, 17);
			this->m_TestLabel->TabIndex = 2;
			this->m_TestLabel->Text = L"commands will appear here";
			// 
			// dbform
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(371, 359);
			this->Controls->Add(this->m_TestLabel);
			this->Controls->Add(this->m_Run);
			this->Controls->Add(this->m_Exit);
			this->Name = L"dbform";
			this->Text = L"USBBoot";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void m_Exit_Click(System::Object^  sender, System::EventArgs^  e)
	{
		Close();
	}
	private: System::Void m_Run_Click(System::Object^  sender, System::EventArgs^  e)
	{
		CmdManager* cmd = new CmdManager();
		cmd->AddCommand("select disk 1");
		cmd->AddCommand("clean");
		cmd->Exec();
		delete cmd;
	}
	};
}
